Benchmarking CI Service
=======================

Brainstorm of Debian Med/SEQwiki/biotools benchmarking service

**This thing needs a name, ASAP!**

Authors
-------

- Piotr Wojtek Dabrowski
- Kevin Murray
- Your name here (please)

Overview
--------

tl;dr: A jenkins.d.n/debci service similar to ReproducibleBuilds project, but
for tool's scientific accuracy.

  - Curate simulated & published datasets
  - Select tools with CWL tool descriptions and EDAM metadata
  - Run CWL workflows in containers when package, data, benchmark or
    dependencies change
  - Inspect results of tool execution with benchmarking metrics
  - Report metrics to Debian infrastructure (UDD) and wider community (SEQWiki,
    bio.tools)

Packages to be tested
---------------------

All packages within debian med that:

  - Have EDAM-compatible DebTags
  - Have a CWL tool description for each tool in the package
    - Should contain EDAM tags per operation
    - Potentially one CWL tool file per subtool/operation (e.g. `samtools view`
      vs `samtools sort`)
  - Are in `main`

Operation
---------

  - Service that runs benchmarks when:
    - There is a new version of the underlying package in Debian med unstable
      - Including any conversion utilities
    - There is a change in an applicable script for the calculation of metrics
    - There is a change in an applicable benchmark dataset
    - There is an applicable transition in progress??
      - Could catch subtle bugs e.g. py3.4 -> py3.5 issues
  - Benchmark datasets and scripts for calculation of metrics are automatically
    selected from repository/DB based on their and the tool's EDAM tags
  - For reproducibility/debugging, a Docker image performing all setup and
    preconversion is published along with the benchmark results


Architecture:
-------------

  - Pre-package and publish to separate local archive (benchmarking specific
    code ONLY):
    - Metric script .debs
    - Real/published dataset .debs
      - From public databases e.g. SRA or RefSeq
      - *Not* for simulated datasets, their parameters are kept within CWL
        files
  - Benchmarking workflows are CWL workflows:
    - Tests run in docker containers:
    - Poll YAML or DB for build-deps and datasets
    - Install tool and tool deps from ftp.debian.org/debian
    - Install required evaluation/metric tools and dataset .debs from our own
      archive
      - Need a way to code these build-deps in a CWL file
    - Create Dockerfile for the workflow?
      - Or is a CWL workflow with dockerised tools enough?
    - Run container of above image, producing data file results
    - Workflow steps:
      - Obtain dataset:
        - Either: a) run simulator steps (log seeds), or b) install data
          package from local repo per above
      - Do any pre-conversion to tool input format (auto-detected from EDAM
        formats)
      - Run tool on data
      - Run any post-conversion tools to evaluation code input format (also
        auto-detected)
      - Run evaluation code
      - Report full result
        - Probably as YAML file, or similar.
      - Report "benchmark status" to UDD
        - Simple state-based update (fail, got worse, all ok)
        - Report biggest change in metric (potentially biggest improvement and
          regression) for all builds
        - There may be many tools per package, report best/worst across all
          tools
        - Path to and checksum of tarball of all benchmark results
        - Will need a new UDD table
  - Docker images
    - Could use CWL tool Docker containers thru CWL workflow
      - Don't always use Debian, unfortunately
    - Could run whole workflow within Debian Unstable docker container
      - But still run CWL workflow within the container
    - Debugging/reproducible containers
      - auto-generated `Dockerfile` for image containing all datasets, metrics,
        conversion tools, and `RUN` steps for obtaining data and running
        pre-conversion (but stopping before tool execution.
      - If we use docker for actual workflow execution, then this is what would
        be used for the test execution
  - Could all this run on a new instance of debci? or Jenkins?

Schema of ideas
---------------

  - There may be many tools per package
  - Each tool may have many benchmarkable operations
  - Each operation of the tool should be tested by many datasets
  - Each test should (or may) report more than one metric


Possible datasets:
------------------

  - https://sites.stanford.edu/abms/giab: Genome In A Bottle  is a NIST human
    NGS resequencing dataset (Paper:
tp://biorxiv.org/content/early/2015/09/15/026468)
  - http://gmisatest.referata.com/wiki/Dataset_1408MLGX6-3WGS
https://public.etherpad-mozilla.org/p/debian-med-benchmarking












`cat brain | less`
------------------

The EDAM classification of a tool lives in the DebTags, and the CWL description
of a tool lives within the debian med package

The definitions and annotations (EDAM) of the datasets and the scripts and EDAM
annotations of the calculation of the metrics live in a git repository and are
synched on a regular basis into a relational database living on the same server
that the packages are generated on. The pages on the SeqWiki are just a view
onto that github repository. The results of the benchmarks also live in the
same relational database and are also synched onto SeqWiki and reported to UDD.

These packages live and are built on a server separate from the official Debian
repositories.

Package definitions are automatically created based on the CWL descriptions of
the tools/toolchains that are to be benchmarked

The packages are built by pbuilder


Requirements for packages in debian med that can be benchmarked:
    - Have an EDAM-compatible DebTag
    - Have a CWL file describing the included tool
On the benchmarking machine we have CWL descriptions for data converters, when
a tool is benchmarked we automatically build a path from the benchmark dataset
file format to the tool's input file format and then from the tool's output
file format to the metric calculation script's input file format


Autopkgtest?

 - It may be worth investigating using autopkgtest infrastruture (perhaps
   run the service as a debci instance) that runs autopakcage tests:
 - each benchmark package contains a test (or tests) in autopkgtest format,
   that we parse & use on our debci



Metadata storage:

 - Repository of YAML-style markup parsed into SQL?
 - Debtags?
 - Just write a script?


UDD:

 - [debmed's page](https://udd.debian.org/dmd/?email1=debian-med-packaging%40lists.alioth.debian.org&email2=&email3=&packages=&ignpackages=&format=html#todo)
 - Or, a more sane example
   [KDM](https://udd.debian.org/dmd/?email1=spam%40kdmurray.id.au&email2=&email3=&packages=&ignpackages=&format=html#todo)
 - New table required for debian benchmarks status data
 - Errors in building a test or major changes in a metric are reported to
   UDD in a fashion similar to how it is done by the ReproducibleBuilds system:
We report whether the test could be computed at all and the largest positive
and negative deviations of scores (in any dataset on any metric), plus a
description of in which dataset and on which metric this deviation has occurred
